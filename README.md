## Pré-requisitos

 - Node.js >= v7
 - npm install yarn -g
   <!-- Instalar Yarn Global -->

## Rodar aplicação

- Instalar dependências  `npm i` e depois `yarn install`
- Rodar `yarn start` (ou `npm start`) desenvolvimento online em `http://localhost:3002`
- Rodar `yarn build` (ou `npm run build`) para gerar o build de produção (Arquivos gerado no diretório `dist`)

## Scripts

- `yarn start` (ou `npm start`) : Iniciar a aplicação
- `yarn build` (ou `npm run build`) : Gerar build de produção
- `yarn update-packages` : Atualizar todas as dependências para a versão atual
